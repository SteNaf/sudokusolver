async function postSudoku(sudoku) {
    return (await fetch('http://localhost:4567/solve', {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(sudoku),
        mode: 'cors'
    }));
}