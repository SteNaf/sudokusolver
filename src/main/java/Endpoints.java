import org.json.JSONArray;

import static spark.Spark.*;

public class Endpoints {
    public Endpoints() {
    }

    public void getEndpoints() {
        options("/*",
                (req, res) -> {

                    String accessControlRequestHeaders = req.headers("Access-Control-Request-Headers");
                    if (accessControlRequestHeaders != null) {
                        res.header("Access-Control-Allow-Headers", accessControlRequestHeaders);
                    }

                    String accessControlRequestMethod = req.headers("Access-Control-Request-Method");
                    if (accessControlRequestMethod != null) {
                        res.header("Access-Control-Allow-Methods", accessControlRequestMethod);
                    }

                    return "OK";
                });

        before((req, res) -> {
            res.header("Access-Control-Allow-Origin", "*");
            res.type("application/json");
        });

        post("/solve", (req, res) -> {
            Solver solver = new Solver(new Sudoku(new JSONArray(req.body())));

            solver.solve();
            return solver.toJSON();
        });

        after((req, res) -> {
            res.status(200);
            res.type("application/json");
        });

        notFound((req, res) -> {
            res.type("application/json");
            res.status(404);
            return "{\"message\":\"404 Not Found\"}";
        });
    }
}
