import org.json.JSONArray;

public class Solver {
    private final int[][] board;

    public Solver(Sudoku sudoku) {
        this.board = sudoku.getBoard();
    }

    private boolean isInRow(int row, int number) {
        for (int i = 0; i < 9; i++)
            if (board[row][i] == number)
                return true;

        return false;
    }

    private boolean isInCol(int col, int number) {
        for (int i = 0; i < 9; i++)
            if (board[i][col] == number)
                return true;

        return false;
    }

    private boolean isInBox(int row, int col, int number) {
        int r = row - row % 3;
        int c = col - col % 3;

        for (int i = r; i < r + 3; i++)
            for (int j = c; j < c + 3; j++)
                if (board[i][j] == number)
                    return true;

        return false;
    }

    private boolean isOk(int row, int col, int number) {
        return !isInRow(row, number) && !isInCol(col, number) && !isInBox(row, col, number);
    }

    public boolean solve() {
        for (int row = 0; row < 9; row++) {
            for (int col = 0; col < 9; col++) {
                if (board[row][col] == 0) {
                    for (int number = 1; number <= 9; number++) {
                        if (isOk(row, col, number)) {
                            board[row][col] = number;

                            if (solve()) {
                                return true;
                            } else {
                                board[row][col] = 0;
                            }
                        }
                    }

                    return false;
                }
            }
        }

        return true;
    }

    public JSONArray toJSON() {
        JSONArray sudoku = new JSONArray();
        for (int y = 0; y < 9; y++) {
            for (int x = 0; x < 9; x++) {
                sudoku.put("" + x + y + board[y][x]);
            }
        }
        return sudoku;
    }

    @SuppressWarnings("unused")
    public void print() {
        for (int y = 8; y >= 0; y--) {
            for (int j = 0; j < 9; j++) {
                System.out.print(" " + board[y][j]);
            }

            System.out.println();
        }

        System.out.println();
    }
}