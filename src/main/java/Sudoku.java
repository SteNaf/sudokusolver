import org.json.JSONArray;

import java.util.InputMismatchException;

public class Sudoku {
    private final int[][] board = new int[9][9];

    public Sudoku(JSONArray sudoku) {
        for (Object o : sudoku) {
            String s = o.toString();
            int y = Integer.parseInt(String.valueOf(s.charAt(0)));
            int x = Integer.parseInt(String.valueOf(s.charAt(1)));
            int value = Integer.parseInt(String.valueOf(s.charAt(2)));
            this.board[x][y] = value;
        }

        if (!isValidSudoku()) {
            throw new InputMismatchException("Wrong sudoku");
        }
    }

    public boolean isValidSudoku() {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[0].length; j++) {
                if (board[i][j] == 0) continue;
                if (!isValid(board, i, j)) return false;
            }
        }
        return true;
    }

    public boolean isValid(int[][] board, int row, int col) {
        for (int i = 0; i < board.length; i++) {
            if (i == row) continue;
            if (board[i][col] == board[row][col]) return false;
        }
        for (int j = 0; j < board[0].length; j++) {
            if (j == col) continue;
            if (board[row][j] == board[row][col]) return false;
        }
        for (int i = (row / 3) * 3; i < (row / 3 + 1) * 3; i++) {
            for (int j = (col / 3) * 3; j < (col / 3 + 1) * 3; j++) {
                if (i == row && j == col) continue;
                if (board[i][j] == board[row][col]) return false;
            }
        }
        return true;
    }

    public int[][] getBoard() {
        return board;
    }
}